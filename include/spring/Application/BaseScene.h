#pragma once

#include <spring\Framework\IScene.h>
#include <qobject.h>
#include<spring\Application\PlottingScene.h>
#include<spring/Application/MonoInput.h>
#include<qaudioinput.h>
namespace Spring
{
	class BaseScene : public IScene
	{
		Q_OBJECT

	public:
		BaseScene(const std::string& ac_szSceneName);

		void createScene() override;

		void release() override;
		void  mp_InitPlotters();

		~BaseScene();
		
  public slots:
      void mp_BackButton();
	  void mf_PlotRandom();
	  void mf_cleanPlot(); 
	  void mf_startTimer();
	  void mf_stopTimer();
	
  private:
		QWidget * centralWidget;
		QCustomPlot * m_customPlot;
		double mv_refreshRate;
		QTimer mv_Timer;
		MonoInput * mMonoInput;
		QAudioInput *m_audioInput;
	};
}
