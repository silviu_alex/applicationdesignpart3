#include <qiodevice.h>
#include <qaudioformat.h>
#include <qvector.h>
class MonoInput : public QIODevice
{
public:
	MonoInput(const unsigned int SampleRate);
	~MonoInput();
	qint64 readData(char *data, qint64 maxLen);
	qint64 writeData(const char *data, qint64 len);
	QAudioFormat getAudioFormat();
	QVector <double> vecGetData();
private:
	QAudioFormat m_audioFormat;
	QVector<double> m_Samples;
};