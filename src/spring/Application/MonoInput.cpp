#include<spring\Application\MonoInput.h>
#include<iostream>
#include<qendian.h>
MonoInput::MonoInput(const unsigned int ac_nSampleRate)
{
	m_audioFormat.setSampleRate(ac_nSampleRate);
	m_audioFormat.setChannelCount(1);
	m_audioFormat.setSampleSize(16);
	m_audioFormat.setSampleType(QAudioFormat::SignedInt);
	m_audioFormat.setByteOrder(QAudioFormat::LittleEndian);
	m_audioFormat.setCodec("audio/pcm");
}
MonoInput::~MonoInput()
{
}

qint64 MonoInput::readData(char * data, qint64 maxLen)
{
	Q_UNUSED(data);
	Q_UNUSED(maxLen);
	return -1;
}

qint64 MonoInput::writeData(const char * data, qint64 len)
{
	const auto *ptr = reinterpret_cast<const unsigned char *>(data);
	m_Samples.clear();
	m_Samples.resize(len / 2);
	for (qint64 i = 0; i < len / 2; i++)
	{
		double value = qFromLittleEndian<qint16>(ptr);
		m_Samples[i] = value;
		ptr += 2;
	}
	return len;
}

QAudioFormat MonoInput::getAudioFormat()
{
	return m_audioFormat;
}

QVector<double> MonoInput::vecGetData()
{
	return m_Samples;
}
