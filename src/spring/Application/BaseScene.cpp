#include <spring\Application\BaseScene.h>

#include "ui_base_scene.h"
namespace Spring
{
	BaseScene::BaseScene(const std::string& ac_szSceneName) : IScene(ac_szSceneName)
	{
	}
	void BaseScene::createScene()
	{
		//create the UI
		const auto ui = std::make_shared<Ui_baseScene>();
		ui->setupUi(m_uMainWindow.get());

		//connect btn's release signal to defined slot
		QObject::connect(ui->backButton, SIGNAL(released()), this, SLOT(mp_BackButton()));
		QObject::connect(ui->startButton, SIGNAL(released()), this, SLOT(mf_startTimer()));
		QObject::connect(ui->stopButton, SIGNAL(released()), this, SLOT(mf_stopTimer()));
		//setting a temporary new title
		m_uMainWindow->setWindowTitle(QString("new title goes here"));

		//setting centralWidget
		centralWidget = ui->centralwidget;
		m_customPlot = ui->widget;
		//Get the title from transient data
		std::string appName = boost::any_cast<std::string>(m_TransientDataCollection["ApplicationName"]);
		mv_refreshRate = boost::any_cast<double>(m_TransientDataCollection["RefreshRate"]);
		mv_Timer.setInterval(1000/mv_refreshRate);
		QObject::connect(&mv_Timer, SIGNAL(timeout()), this, SLOT(mf_PlotRandom()));
		
		m_uMainWindow->setWindowTitle(QString(appName.c_str()));
		mp_InitPlotters();

		//Audio management
		unsigned int sample_rate = boost::any_cast<unsigned int>(m_TransientDataCollection["SampleRate"]);
		mMonoInput = new MonoInput(sample_rate);
	}
	void BaseScene::release()
	{
		//mf_cleanPlot();
		mf_stopTimer();
		delete centralWidget;
	}

	void BaseScene::mp_InitPlotters()
	{
		m_customPlot->setInteraction(QCP::iRangeZoom, true);
		m_customPlot->setInteraction(QCP::iRangeDrag, true);

		m_customPlot->addGraph();
		m_customPlot->graph(0)->setLineStyle(QCPGraph::LineStyle::lsLine);
		m_customPlot->xAxis->setLabel("sec");
		m_customPlot->yAxis->setLabel("V");
	}

	BaseScene::~BaseScene()
	{

	}

	void BaseScene::mf_PlotRandom()
	{
		QVector<double> values = mMonoInput->vecGetData();
		const int NO_OF_SAMPLES = values.length();
		QVector<double> y(NO_OF_SAMPLES);
		QVector<double> x(NO_OF_SAMPLES);
		x[0] = 0;
		for (int i = 0; i < NO_OF_SAMPLES; i++)
		{
			x[i] = i;
			//y[i] = rand() % 20;
		}

		m_customPlot->graph(0)->setData(x, values);
		m_customPlot->rescaleAxes();
		m_customPlot->replot();
	}

	void BaseScene::mf_cleanPlot()
	{
		m_customPlot->graph(0)->data()->clear();
		//m_customPlot->rescaleAxes();
		m_customPlot->replot();
	}

	void BaseScene::mf_startTimer()
	{
		m_audioInput = new QAudioInput(mMonoInput->getAudioFormat());
		mMonoInput->open(QIODevice::WriteOnly);
		m_audioInput->start(mMonoInput);
		mv_Timer.start();
	}

	void BaseScene::mf_stopTimer()
	{
		m_audioInput->stop();
		mMonoInput->close();
		mv_Timer.stop();
		mf_cleanPlot();
	}

	void BaseScene::mp_BackButton()
  {
    const std::string c_szNextSceneName = "Initial scene";
    emit SceneChange(c_szNextSceneName);
  }

}
